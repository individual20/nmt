from flask import Flask
from flask import render_template
from form import input_form

from bert_fused_translate import translate as bert_fused_translate
from transformer_based.translate import translate as transformer_translate


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'


@app.route('/',methods=['GET'])
def dictionary(): 
    form = input_form()
    return render_template("submit_output.html",form=form)


@app.route('/',methods=['POST'])
def match():
    form = input_form()
    
    if form.validate_on_submit():
        vi_sentence = form.word.data.lower()
        selected_model = form.model.data
        bana_sentence = ''
        if selected_model == 'bert-fused':
            bana_sentence = bert_fused_translate(vi_sentence)
        else:
            bana_sentence = transformer_translate(vi_sentence)
        return render_template("submit_output.html",form=form,bana_sentence=bana_sentence)
