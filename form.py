from flask_wtf import FlaskForm
from wtforms import StringField,SubmitField, SelectField
from wtforms.validators import DataRequired

class input_form(FlaskForm):

    word = StringField(validators=[DataRequired()])
    model = SelectField('Model', choices=[('bert-fused', 'Bert-fused'), ('transformer', 'Transformer')])
    submit = SubmitField('Translate')


